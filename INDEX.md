# Defrag

Defragmentation utility.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DEFRAG.LSM

<table>
<tr><td>title</td><td>Defrag</td></tr>
<tr><td>version</td><td>1.3.2a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-06-13</td></tr>
<tr><td>description</td><td>Defragmentation utility.</td></tr>
<tr><td>keywords</td><td>defragmenter</td></tr>
<tr><td>author</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://users.telenet.be/imre/FreeDOS/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://users.pandora.be/imre/FreeDOS/</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/defrag/</td></tr>
<tr><td>original&nbsp;site</td><td>http://users.telenet.be/imre/FreeDOS/</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Defrag</td></tr>
<tr><td>platforms</td><td>FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, version 2</td></tr>
</table>
